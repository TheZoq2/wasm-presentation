#import "./typst-presentation-common/theme.typ" : *

#show: university-theme.with()

#slide(no-margins: true)[
  #only(1, image("./slide1.png", width:100%))
  #only(2, image("./slide2.png", width:100%))
  #only(3, image("./slide3.png", width:100%))
  #only(4, image("./slide4.png", width:100%))
  #only(5, image("./slide5.png", width:100%))
  #only(6, image("./slide6.png", width:100%))
  #only(7, image("./slide7.png", width:100%))
]
